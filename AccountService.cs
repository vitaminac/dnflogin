﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace DNFLoginLauncher
{
    public class AccountService
    {
        private static readonly string MYSQL_CONNECTION_STRING = @"server=192.168.50.10;userid=game;password=uu5!^%jg;";
        public static async Task<bool> DoesAccountExist([MaybeNull] string username)
        {
            ValidateUsername(username);
            using (var conn = new MySqlConnection(MYSQL_CONNECTION_STRING))
            {
                await conn.OpenAsync();
                const string sql = "SELECT COUNT(*) FROM d_taiwan.accounts WHERE accountname=@Username";
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@Username", username);
                    uint n = Convert.ToUInt32(await cmd.ExecuteScalarAsync());
                    return n != 0;
                }
            }
        }

        public static async Task CreateAccount([MaybeNull] string username, [MaybeNull] string password)
        {
            ValidateUsername(username);
            ValidatePassword(password);
            using (var conn = new MySqlConnection(MYSQL_CONNECTION_STRING))
            {
                await conn.OpenAsync();
                using (var cmd = new MySqlCommand("INSERT INTO d_taiwan.accounts (UID,accountname,password) VALUES (@UID,@Username,@Password)", conn))
                {
                    cmd.Parameters.AddWithValue("@UID", null);
                    cmd.Parameters.AddWithValue("@Username", username);
                    cmd.Parameters.AddWithValue("@Password", password);
                    if (await cmd.ExecuteNonQueryAsync() != 1)
                    {
                        throw new ApplicationException("Failed to register on step 1");
                    }
                }

                ushort uid = await GetUID(username, password);
                using (var cmd = new MySqlCommand("INSERT INTO d_taiwan.member_white_account (m_id) VALUES (@UID)", conn))
                {
                    cmd.Parameters.AddWithValue("@UID", uid);
                    if (await cmd.ExecuteNonQueryAsync() != 1)
                    {
                        throw new ApplicationException("Failed to register on step 2");
                    }
                }
                using (var cmd = new MySqlCommand("INSERT INTO d_taiwan.member_info (m_id,user_id) VALUES (@UID,@UID)", conn))
                {
                    cmd.Parameters.AddWithValue("@UID", uid);
                    if (await cmd.ExecuteNonQueryAsync() != 1)
                    {
                        throw new ApplicationException("Failed to register on step 3");
                    }
                }
                using (var cmd = new MySqlCommand("INSERT INTO taiwan_login.member_login (m_id) VALUES (@UID)", conn))
                {
                    cmd.Parameters.AddWithValue("@UID", uid);
                    if (await cmd.ExecuteNonQueryAsync() != 1)
                    {
                        throw new ApplicationException("Failed to register on step 4");
                    }
                }
            }
        }

        public static async Task<ushort> GetUID([MaybeNull] string username, [MaybeNull] string password)
        {
            ValidateUsername(username);
            ValidatePassword(password);
            using (var conn = new MySqlConnection(MYSQL_CONNECTION_STRING))
            {
                await conn.OpenAsync();
                const string sql = "SELECT UID FROM d_taiwan.accounts WHERE accountname=@Username AND password=@Password";
                using (var cmd = new MySqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@Username", username);
                    cmd.Parameters.AddWithValue("@Password", password);
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        if (!reader.HasRows)
                        {
                            throw new ApplicationException("username or password is incorrect");
                        }

                        await reader.ReadAsync();
                        return Convert.ToUInt16(reader.GetInt32(0));
                    }
                }
            }
        }

        private static void ValidateUsername(in string username)
        {
            if (username == null || username.Trim().Length == 0)
            {
                throw new ArgumentException("cannot be empty", nameof(username));
            }
        }

        private static void ValidatePassword(in string password)
        {
            if (password == null || password.Trim().Length == 0)
            {
                throw new ArgumentException("cannot be empty", nameof(password));
            }
        }
    }
}
