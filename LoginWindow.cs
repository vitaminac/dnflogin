using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace DNFLoginLauncher
{
    public partial class LoginWindow : Form
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [System.STAThread]
        static void Main()
        {
            ApplicationConfiguration.Initialize();
            Application.Run(new LoginWindow());
        }

        public LoginWindow()
        {
            InitializeComponent();
            LoadLoginWindowHTML();
        }

        private void LoadLoginWindowHTML()
        {
            this.webBrowser.ScriptErrorsSuppressed = true;
            this.webBrowser.ObjectForScripting = new LoginWindowScriptCommand(this);
            this.webBrowser.Navigate("about:blank");
            this.webBrowser.Document.OpenNew(false);
            this.webBrowser.Document.Write(Properties.Resources.LoginWindow);
            this.webBrowser.Refresh();
        }

        private void OnWebBrowserDocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            Debug.WriteLine("浏览器加载完成");
        }

        private void OnSelectFolder(object sender, System.EventArgs e)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.folder.Text = folderDlg.SelectedPath;
            }
        }

        public async System.Threading.Tasks.Task Login(string username, string password)
        {
            try
            {
                if (!await AccountService.DoesAccountExist(username))
                {
                    await AccountService.CreateAccount(username, password);
                }

                var UID = await AccountService.GetUID(username, password);
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = $"{this.folder.Text}\\dnf.exe";
                startInfo.WorkingDirectory = this.folder.Text;
                startInfo.Arguments = UserAuth.GetUserAuthToken(UID);
                Process.Start(startInfo);
            }
            catch (Exception ex)
            {
                Alert(ex.Message);
            }
        }

        private void Alert(in string message)
        {
            webBrowser.Document.InvokeScript("Alert", new object[] { message });
        }
    }
}