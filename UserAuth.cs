﻿using System;

namespace DNFLoginLauncher
{
    public class UserAuth
    {
        public static string GetUserAuthToken(in ushort uid)
        {
            string suffix = "010101010101010101010101010101010101010101010101010101010101010155914510010403030101";
            string uidToken = Convert.ToString(uid, 16).ToUpper().PadLeft(8, '0');
            byte[] buffer = HexStringToBytes(uidToken + suffix);
            return RSA.Encrypt(buffer);
        }

        private static byte[] HexStringToBytes(string hexString)
        {
            var returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
            {
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            }
            return returnBytes;
        }
    }
}
