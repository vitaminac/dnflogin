## 配置客户端

1. 运行 `git clone https://gitlab.com/vitaminac/dnfclient`
1. 把服务器同样的`Script.pvf`放到`dnfclient`目录里
1. 下载[补丁大合集](https://gitlab.com/vitaminac/dnfpatch)然后解压到`dnfclient`目录里
1. 编译这个项目然后把`bin\Debug\net6.0-windows`里面的内容复制到`dnfclient`
1. 把服务器同样的`privatekey.pem`复制到`dnfclient`
1. 把`bootstrap.ps1`复制到`dnfclient`然后运行

点击DNF登陆器然后输入用户密码

## References

* [DNF-LoginLauncher](https://github.com/skypub/DNF-LoginLauncher)
* [dnf-client-public](https://github.com/onlyGuo/dnf-client-public)
* [dnf-server-public](https://github.com/onlyGuo/dnf-server-public)
* [dnf-server-web-public](https://github.com/onlyGuo/dnf-server-web-public)
* [dnfLogin](https://github.com/zuopucuen/dnfLogin)
* [滑里稽登陆器](https://github.com/nnn149/DofLauncher)