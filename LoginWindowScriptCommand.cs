﻿using System.Threading.Tasks;

namespace DNFLoginLauncher
{
    [System.Runtime.InteropServices.ComVisible(true)]
    public class LoginWindowScriptCommand
    {
        private readonly LoginWindow loginWindow;

        public LoginWindowScriptCommand(in LoginWindow loginWindow)
        {
            this.loginWindow = loginWindow;
        }

        public async Task Login(string username, string password)
        {
            await this.loginWindow.Login(username, password);
        }
    }
}
