﻿using System;
using System.IO;

namespace DNFLoginLauncher
{
    public class RSA
    {
        public static string Encrypt(in byte[] data)
        {
            var rsaKey = OpenSSL.Crypto.CryptoKey.FromPrivateKey(ReadPrivateKeyFromFile(), null);
            var rsa = rsaKey.GetRSA();
            byte[] encryptedBuffer = rsa.PrivateEncrypt(data, OpenSSL.Crypto.RSA.Padding.PKCS1);
            return Convert.ToBase64String(encryptedBuffer);
        }

        private static string ReadPrivateKeyFromFile()
        {
            return File.ReadAllText(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), Properties.Resources.PrivateKeyFileName));
        }
    }
}
