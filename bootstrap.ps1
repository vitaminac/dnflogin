Write-Host "Ignored Open File – Security Warnings"
Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope LocalMachine -Force
Write-Host "Modified Hosts File"
Add-Content "C:\Windows\System32\drivers\etc\hosts" "`n192.168.50.10 start.dnf.tw`n"
Write-Host "Configuring DNF Client"
$dnfClientFolder = (Get-Location).path
$dofPatchConfig = "$dnfClientFolder/DNF.toml"
$dofPatchConfigContent = [System.IO.File]::ReadAllText("$dofPatchConfig")
$dofPatchConfigContent = $dofPatchConfigContent.Replace("192.168.200.131", "192.168.50.10")
$dofPatchConfigContent = $dofPatchConfigContent.Replace("95", "70")
[System.IO.File]::WriteAllText("$dofPatchConfig", $dofPatchConfigContent)
$desktop = [System.Environment]::GetFolderPath('Desktop')
New-Item -ItemType SymbolicLink -Force -Path "$desktop" -Name "DNFLogin.lnk" -Value "$dnfClientFolder\DNFLoginLauncher.exe"