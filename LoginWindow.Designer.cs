﻿using System.Windows.Forms;

namespace DNFLoginLauncher
{
    partial class LoginWindow
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.loginPage = new System.Windows.Forms.TabPage();
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.configPage = new System.Windows.Forms.TabPage();
            this.folder = new System.Windows.Forms.TextBox();
            this.selectFolder = new System.Windows.Forms.Button();
            this.tabControl.SuspendLayout();
            this.loginPage.SuspendLayout();
            this.configPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.loginPage);
            this.tabControl.Controls.Add(this.configPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(700, 338);
            this.tabControl.TabIndex = 0;
            // 
            // loginPage
            // 
            this.loginPage.Controls.Add(this.webBrowser);
            this.loginPage.Location = new System.Drawing.Point(4, 24);
            this.loginPage.Name = "loginPage";
            this.loginPage.Size = new System.Drawing.Size(692, 310);
            this.loginPage.TabIndex = 0;
            this.loginPage.Text = "登录";
            // 
            // webBrowser
            // 
            this.webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser.Location = new System.Drawing.Point(0, 0);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(692, 310);
            this.webBrowser.TabIndex = 0;
            // 
            // configPage
            // 
            this.configPage.Controls.Add(this.folder);
            this.configPage.Controls.Add(this.selectFolder);
            this.configPage.Location = new System.Drawing.Point(4, 24);
            this.configPage.Name = "configPage";
            this.configPage.Size = new System.Drawing.Size(692, 310);
            this.configPage.TabIndex = 1;
            this.configPage.Text = "设置";
            // 
            // folder
            // 
            this.folder.Enabled = false;
            this.folder.Location = new System.Drawing.Point(208, 30);
            this.folder.Name = "folder";
            this.folder.Size = new System.Drawing.Size(456, 23);
            this.folder.TabIndex = 1;
            this.folder.Text = "C:\\vagrant\\client\\dnf";
            // 
            // selectFolder
            // 
            this.selectFolder.Location = new System.Drawing.Point(25, 30);
            this.selectFolder.Name = "selectFolder";
            this.selectFolder.Size = new System.Drawing.Size(146, 23);
            this.selectFolder.TabIndex = 0;
            this.selectFolder.Text = "DNF游戏路径";
            this.selectFolder.UseVisualStyleBackColor = true;
            this.selectFolder.Click += new System.EventHandler(this.OnSelectFolder);
            // 
            // LoginWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 338);
            this.Controls.Add(this.tabControl);
            this.Name = "LoginWindow";
            this.Text = "DNF单机登录器";
            this.tabControl.ResumeLayout(false);
            this.loginPage.ResumeLayout(false);
            this.configPage.ResumeLayout(false);
            this.configPage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl;
        private TabPage loginPage;
        private TabPage configPage;
        private WebBrowser webBrowser;
        private Button selectFolder;
        private TextBox folder;
    }
}